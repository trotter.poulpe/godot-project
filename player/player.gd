extends CharacterBody3D

@onready var camera_rotating_point:Node3D = $cameraRotatingPoint
@onready var animation_tree:AnimationTree = $AnimationTree
@onready var audio_stream_player_3d_voice: AudioStreamPlayer3D = $voice
@onready var audio_stream_player_3d_sound_effects: AudioStreamPlayer3D = $sound_effects
@onready var dash_raycast: RayCast3D = $DashRaycast
@onready var detect_small_box_top_raycast: RayCast3D = $detectSmallBoxTopRaycast
@onready var detect_small_box_bottom_raycast: RayCast3D = $detectSmallBoxBottomRaycast




enum Player_action {IDLE, RUN, BACKRUN, JUMP, SLASH}

var current_action:Player_action = Player_action.IDLE

const SPEED:float = 5.0
const JUMP_VELOCITY:float = 2.0
var dash_speed: float = 1.5;
var jump_horizon_bonus_speed: float=1
var ms_before_ending_dash: float = 100;
var max_dash_energy:float = 2
var dash_energy:float = max_dash_energy
var MAX_VERTICAL_JUMP_ENERGY:float = 100
var vertical_jump_energy:float = MAX_VERTICAL_JUMP_ENERGY
const INTERVAL_BETWEEN_DASH_SOUND_EFFECT:float = 200
var ms_left_before_next_possible_dash_sound_effect:float = INTERVAL_BETWEEN_DASH_SOUND_EFFECT
var interval_between_dashes:float = 45
var ms_left_before_next_possible_dash:float = interval_between_dashes
var ms_before_ending_bunny_hop:float = 0
const MS_BETWEEN_BUNNY_HOP:float = 30
const BUNNY_HOP_HORIZONTAL_SPEED_BONUS:float = 0.4
var air_control_malus:bool = false
var air_control:float = 0.5
var got_in_air_during_dash:bool = false
var is_dashing:bool = false

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity:Variant = ProjectSettings.get_setting("physics/3d/default_gravity")
@export var sensivity_horizontal:float = 0.2;
@export var sensivity_vertical:float = 0.2;

func _ready() -> void:
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	
func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		rotate_y(deg_to_rad(-event.relative.x * sensivity_horizontal))
		camera_rotating_point.rotate_x(deg_to_rad(-event.relative.y * sensivity_vertical))

func _physics_process(delta:float) -> void:
	ms_left_before_next_possible_dash = move_toward(ms_left_before_next_possible_dash, 0, 100 * delta)
	vertical_jump_energy = move_toward(vertical_jump_energy, 0, 100 * delta)
	ms_before_ending_dash = move_toward(ms_before_ending_dash, 0, 100 * delta)
	ms_before_ending_bunny_hop = move_toward(ms_before_ending_bunny_hop, 0, 100 * delta)
	ms_left_before_next_possible_dash_sound_effect -= move_toward(ms_left_before_next_possible_dash_sound_effect, 0, 100 * delta)
	#print(velocity.y)
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta
		if dash_speed > 1: # if we were dashing and not on floor...
			ms_before_ending_dash = 100 # ...we continue the dash speed...
			jump_horizon_bonus_speed = BUNNY_HOP_HORIZONTAL_SPEED_BONUS#....but not at full speed
		if dash_energy> 0 && ms_before_ending_dash > 0:
			got_in_air_during_dash = true

	# Get the input direction and handle the movement/deceleration.
	var input_dir:Vector2 = Input.get_vector("left", "right", "forward", "backward")
	var direction:Vector3 = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	var desired_velocity:Vector3 = Vector3.ZERO
	handle_dash(delta)
	#	dash_energy += 1 * delta
	if direction:
		if(input_dir.y == -1):
			current_action = Player_action.RUN
		else:
			current_action = Player_action.BACKRUN
		if !air_control_malus:
			desired_velocity.x = direction.x * SPEED * dash_speed * jump_horizon_bonus_speed
			desired_velocity.z = direction.z * SPEED * dash_speed * jump_horizon_bonus_speed
		if air_control_malus:
			desired_velocity.x = (direction.x*air_control) * SPEED * dash_speed * jump_horizon_bonus_speed
			desired_velocity.z = (direction.z*air_control) * SPEED * dash_speed * jump_horizon_bonus_speed
	else:
		desired_velocity.x = move_toward(velocity.x, 0, SPEED)
		desired_velocity.z = move_toward(velocity.z, 0, SPEED)
		current_action = Player_action.IDLE
	
	dash_raycast.target_position = to_local(global_position + velocity.normalized()*0.1)
	dash_raycast.force_raycast_update()
	print(direction)

	# The raycast goes toward where we try to move
	# Top a bit longer to not consider sloes small boxes
	detect_small_box_bottom_raycast.target_position = to_local(global_position + direction.normalized()*0.1)
	detect_small_box_top_raycast.target_position = to_local(global_position + direction.normalized()*0.16)
		
	if detect_small_box_bottom_raycast.is_colliding() && !detect_small_box_top_raycast.is_colliding():
		print("faut monter")
		velocity.y = delta * SPEED * 22

	# slow down the player if a raycast collide a wall
	# to prevent the player to go through a thin wall
	if dash_raycast.is_colliding() && is_dashing:
		var distance_to_collision: float = (dash_raycast.get_collision_point() - global_position).length()
		var current_speed:float = desired_velocity.length()
		if current_speed > distance_to_collision:
			print("slow down")
			var limited_velocity:Vector3 = desired_velocity.normalized() * distance_to_collision
			velocity.x = desired_velocity.x/3
			velocity.z = desired_velocity.z/3
		else:
			# If we're not going to overshoot, use the full desired velocity
			velocity.x = desired_velocity.x
			velocity.z = desired_velocity.z
	else:
		velocity.x = desired_velocity.x
		velocity.z = desired_velocity.z


	handle_jump(delta)
	#print(Player_action.keys()[current_action])
	#prints(velocity.length())
	handle_anim()
	move_and_slide()
	set_max_slides(200)


#Transitions between anims.
func handle_anim() -> void:
	if Input.is_action_just_pressed("attack") :
		current_action = Player_action.SLASH
	match current_action:
		Player_action.IDLE:
			animation_tree.set("parameters/Transition/transition_request", "idle")
		Player_action.RUN:
			animation_tree.set("parameters/Transition/transition_request", "sprint")
		Player_action.BACKRUN:
			animation_tree.set("parameters/Transition/transition_request", "backrun")
		Player_action.JUMP:
			animation_tree.set("parameters/jump/request", AnimationNodeOneShot.ONE_SHOT_REQUEST_FIRE)
		Player_action.SLASH:
			animation_tree.set("parameters/slash/request", AnimationNodeOneShot.ONE_SHOT_REQUEST_FIRE)
func handle_dash(delta: float) -> void:
	if Input.is_action_just_pressed("dash") and is_on_floor() and ms_left_before_next_possible_dash==0:
		play_sound_effect("res://player/sound/woosh.mp3");
		play_voice("res://player/sound/dash" + str(randi_range(1, 3)) +".mp3");
		dash_energy = max_dash_energy
		got_in_air_during_dash = false
		ms_left_before_next_possible_dash = interval_between_dashes
	if Input.is_action_pressed("dash") and is_on_floor() && ms_before_ending_bunny_hop == 0:
		if dash_energy <= 0:
			dash_speed = 1;
		if dash_energy > 0:
			dash_speed = 8;
			ms_before_ending_dash = 100
			dash_energy -= 10 * delta
			is_dashing = true
			if dash_energy < 0: 
				dash_energy = 0
		if got_in_air_during_dash:
			dash_speed = 1;
	if !Input.is_action_pressed("dash") && ms_before_ending_bunny_hop == 0:
		dash_speed = 1;
		dash_energy = 0
	if dash_speed == 1: is_dashing = false
	
func handle_jump(delta:float) -> void:
	if is_on_floor():
		air_control_malus = false
	if velocity.length() > 10 && !is_on_floor(): #if we go very fast, for instance tumbling downhill...
		ms_before_ending_bunny_hop = MS_BETWEEN_BUNNY_HOP #...the MS_BETWEEN_BUNNY_HOP doesn't apply anymore
	# Handle jump.
	if Input.is_action_just_released("backward") || Input.is_action_just_released("forward") || Input.is_action_just_released("left") || Input.is_action_just_released("right"):
		air_control_malus = true
	if Input.is_action_just_released("jump") and vertical_jump_energy>0:
		vertical_jump_energy = 0
	if Input.is_action_pressed("jump") and vertical_jump_energy>0 and !is_on_floor():
		#I have to cheat a bit if not the jump seems "stuck" on top of his arc
		velocity.y = (JUMP_VELOCITY*(vertical_jump_energy/MAX_VERTICAL_JUMP_ENERGY))*3

	if Input.is_action_just_pressed("jump") and is_on_floor():
		vertical_jump_energy = MAX_VERTICAL_JUMP_ENERGY
		play_voice("res://player/sound/jump" + str(randi_range(1, 1)) +".mp3");
		velocity.y = JUMP_VELOCITY
		current_action = Player_action.JUMP
		if dash_energy> 0 && ms_before_ending_dash > 0:
			ms_before_ending_bunny_hop = MS_BETWEEN_BUNNY_HOP
			# we get a bonus forward speed if jump during dash
			jump_horizon_bonus_speed = BUNNY_HOP_HORIZONTAL_SPEED_BONUS
			dash_energy = max_dash_energy # cannot run out of dash energy in bunnyhop
	if !Input.is_action_just_pressed("jump") and is_on_floor() && ms_before_ending_bunny_hop <= 0:
		jump_horizon_bonus_speed = 1
	
func play_voice(res:String)->void:
		audio_stream_player_3d_voice.volume_db = -2
		audio_stream_player_3d_voice.bus = "Master"
		audio_stream_player_3d_voice.stream = load(res)
		audio_stream_player_3d_voice.pitch_scale = randf_range(0.99, 1.02)
		audio_stream_player_3d_voice.play()

func play_sound_effect(res:String)->void:
		audio_stream_player_3d_sound_effects.volume_db = -2
		audio_stream_player_3d_sound_effects.bus = "Master"
		audio_stream_player_3d_sound_effects.stream = load(res)
		audio_stream_player_3d_sound_effects.pitch_scale = randf_range(0.99, 1.02)
		audio_stream_player_3d_sound_effects.play()
